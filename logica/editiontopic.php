<?php
require "persistencia/EditionTopicDAO.php";

class EditionTopic{
    private $idEditionTopic;
    private $accepted;
    private $rejected;
    private $idEdition;
    private $conexion;
    private $EditionTopicDAO;

   

    public function getIdEditionTopic()
    {
        return $this->idEditionTopic;
    }
    
    public function getAccepted()
    {
        return $this->accepted;
    }
    
    public function getRejected()
    {
        return $this->rejected;
    }

    public function getIdEdition()
    {
        return $this->idEdition;
    }

    function EditionTopic ($pidEditionTopic="", $paccepted="", $pyear="", $prejected= "", $pidEdition="") {
        $this -> idEditionTopic = $pidEditionTopic;
        $this -> accepted = $paccepted;
        $this -> rejected = $prejected;
        $this -> idEdition = $pidEdition;
        $this -> conexion = new Conexion();
        $this -> EditionTopicDAO = new EditionTopicDAO($pidEditionTopic, $paccepted, $pyear, $prejected, $pidEdition);        
    }

    function consultarproyectos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> EditionTopicDAO -> consultarproyectos());
        $this -> conexion -> cerrar();
        $resultados = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($resultados, array($resultado[0],$resultado[1],$resultado[2]));
        }
        return $resultados;
    }

}