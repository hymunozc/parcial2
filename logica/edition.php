<?php
require "persistencia/EditionDAO.php";

class Edition{
    private $idEdition;
    private $name;
    private $year;
    private $startDate;
    private $endDate;
    private $internationalCollaboration;
    private $numberOfKeynotes;
    private $conexion;
    private $EditionDAO;

    public function getIdEdition()
    {
        return $this->idEdition;
    }

    public function getName()
    {
        return $this->name;
    }
    
    public function getYear()
    {
        return $this->year;
    }
    
    public function getStartDate()
    {
        return $this->startDate;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    function Edition ($pidEdition="", $pname="", $pyear="") {
        $this -> idEdition = $pidEdition;
        $this -> name = $pname;
        $this -> year = $pyear;
        $this -> conexion = new Conexion();
        $this -> EditionDAO = new EditionDAO($pidEdition, $pname,$pyear);        
    }

    function consultarAños(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> EditionDAO -> consultarAños());
        $this -> conexion -> cerrar();
        $años = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($años, new Edition($resultado[0]));
        }
         return  $años;
    }
}


    