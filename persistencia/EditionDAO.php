<?php

class EditionDAO{
    private $idEdition;
    private $name;
    private $year;
    private $startDate;
    private $endDate;
    private $internationalCollaboration;
    private $numberOfKeynotes;
    private $conexion;
    private $EditionDAO;

    public function getIdEdition()
    {
        return $this->idEdition;
    }

    public function getName()
    {
        return $this->name;
    }
    
    public function getYear()
    {
        return $this->year;
    }
    
    public function getStartDate()
    {
        return $this->startDate;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    function Edition ($pidEdition="", $pname="", $pyear="") {
        $this -> idEdition = $pidEdition;
        $this -> name = $pname;
        $this -> year = $pyear;       
    }

    function consultarAños(){
        return "select year
                from edition";
    }

}


    