<?php
require "persistencia/EditionTopicDAO.php";

class EditionTopic{
    private $idEditionTopic;
    private $accepted;
    private $rejected;
    private $idEdition;
    private $conexion;
    private $EditionTopicDAO;

   

    public function getIdEditionTopic()
    {
        return $this->idEditionTopic;
    }
    
    public function getAccepted()
    {
        return $this->accepted;
    }
    
    public function getRejected()
    {
        return $this->rejected;
    }

    public function getIdEdition()
    {
        return $this->idEdition;
    }

    function EditionTopic ($pidEditionTopic="", $paccepted="", $pyear="", $prejected= "", $pidEdition="") {
        $this -> idEditionTopic = $pidEditionTopic;
        $this -> accepted = $paccepted;
        $this -> rejected = $prejected;
        $this -> idEdition = $pidEdition;       
    }

    function consultarproyectos(){
        return "select e.edition_idEdition as año, SUM(accepted), sum(rejected)
                from edition a join editiontopic e on (a.idEdition =  e.edition_idEdition)
                group by accepted, rejected
                order by año DESC"
    }

}