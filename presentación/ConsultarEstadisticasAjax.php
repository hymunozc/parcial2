<?php
    $Etopic = new EditionTopic(); 
    $estadisticas = $Etopic -> consultarLibrosPorAutor();
    $datos = "['aceptado', 'rechazado'], ";
    foreach ($estadisticas as $l){
        $datos .= "['" . trim($l[0]) . " " . trim($l[1]) . "', " . $l[2] . "], ";
    }
    $datos = substr($datos, 0, strlen($datos) - 1);
?>
<div class="container">
	<div class="row mt-3">
		<div class="col-2"></div>
		<div class="col-8">
			<div class="card">
				<div class="card-header">
					<h3>Estadisticas proyectos</h3>
				</div>
				<div class="card-body">
					<div class="text-center" id="piechartCantidad" style="width: 600px; height: 500px;"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    
    function drawChart() {
    
        var data = google.visualization.arrayToDataTable([
            <?php echo $datos ?>
            
        ]);
        
        var options = {
        	title : "Proyectos por año"
        };                            
        var chart = new google.visualization.PieChart(document.getElementById('piechartCantidad'));                                
        chart.draw(data, options);
    }
</script>
<?php 