
<div class="container">
	<div class="row mt-3">
		<div class="col-2"></div>
		<div class="col-8">
			<div class="card">
				<div class="card-header">
                <h3>Estadisticas </h3>
				</div>
				<div class="card-body">
                <div class="input-group mb-3 text-center">
                <label class="input-group-text" for="inputGroupSelect01">Edición</label>
                <select class="form-select" id="seleccionar">
                <?php 
    							$edition = new Edition();
    							$ediciones =  $edition -> consultarAños();
    							foreach($ediciones as $edicionActual){
                                    echo "<option value='" . $edicionActual -> getYear() . "'</option>";
								}  						
    		    ?> 
</div>
<div class="row mt-3">
		<div class="col">
			<div id="resultados"></div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$("#seleccionar").keyup(function(){
		if($("#seleccionar").val().length > 0){
			url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/autor/ConsultarEstadisticasAjax.php") ?>&seleccionar=" + $("#seleccionar").val();
			$("#resultados").load(url);
		}
	});
});
</script>